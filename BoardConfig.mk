#
# Copyright (C) 2021 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from common sdm439-common
include device/samsung/sdm439-common/BoardConfigCommon.mk

DEVICE_PATH := device/samsung/m01q

# Kernel


# Partitions

# Security patch level
VENDOR_SECURITY_PATCH := 2021-04-01

# Inherit from the proprietary version
include vendor/samsung/m01q/BoardConfigVendor.mk
