#
# Copyright (C) 2021 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common LineageOS stuff.
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/handheld_system_ext.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/telephony_system_ext.mk)

# Inherit from pine device
$(call inherit-product, device/samsung/m01q/device.mk)

# Device identifier. This must come after all inclusions
PRODUCT_DEVICE := m01q
PRODUCT_NAME := lineage_m01q
BOARD_VENDOR := Samsung
PRODUCT_BRAND := Samsung
PRODUCT_MODEL := Samsung Galaxy M01
PRODUCT_MANUFACTURER := Samsung
TARGET_VENDOR := Samsung

PRODUCT_GMS_CLIENTID_BASE := android-samsung

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="m01qins-user 11 RP1A.200720.012 release-keys"

# Set BUILD_FINGERPRINT variable to be picked up by both system and vendor build.prop
BUILD_FINGERPRINT := "samsung/m01qins/m01q:11/RP1A.200720.012/M015GXXU3BUD9:user/release-keys"
